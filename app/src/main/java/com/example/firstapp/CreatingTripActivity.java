package com.example.firstapp;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.Location;
import com.example.firstapp.model.Trip;
import com.example.firstapp.model.User;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.model.TypeFilter;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.LatLng;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;
import org.joda.time.LocalTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class CreatingTripActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseTrips;

    private User customer = new User();
    private List<Driver> availableDrivers = new ArrayList<>();

    private EditText numOfPsg;
    private RadioGroup radioGroup;
    private CheckBox optionPetFriendly;
    private CheckBox optionEnglishSpeaking;
    private CheckBox optionBabySeat;

    private String start;

    private AutocompleteSupportFragment autocompleteInput;
    private AutocompleteSupportFragment autocompleteEndInput;

    private String addressStartingInput = "";
    private String addressEndingInput = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creating_trip);

        numOfPsg = findViewById(R.id.numberOfPassengers);
        radioGroup = findViewById(R.id.CarTypeRadio);
        optionPetFriendly = findViewById(R.id.btnPetFriendly);
        optionEnglishSpeaking = findViewById(R.id.btnEnglishSpeaking);
        optionBabySeat = findViewById(R.id.btnBabySeat);
        start = "Chosen address";
        if (getIntent().hasExtra("com.example.firstapp.START_POINT")){
            start = getIntent().getStringExtra("com.example.firstapp.START_POINT");
        }

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseTrips = FirebaseDatabase.getInstance().getReference().child("Trips");

        DatabaseReference ref = mDatabase.child("Users").child(mAuth.getCurrentUser().getUid());

        ref.addListenerForSingleValueEvent(new SingleEventCurrentUserReaderListener());

        DatabaseReference refDriver = mDatabase.child("Drivers");
        refDriver.addValueEventListener(new DriverValueListener());

        Places.initialize(this, getString(R.string.directApiKey));
        PlacesClient placesClient = Places.createClient(this);

    }

    @Override
    protected void onStart(){
        super.onStart();

        // Autocomplete input fields
        autocompleteInput = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        autocompleteInput.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS));
        autocompleteInput.setHint("Start Location");
        autocompleteInput.setTypeFilter(TypeFilter.ADDRESS);
        autocompleteInput.setCountry("RS");
        autocompleteInput.setOnPlaceSelectedListener(new PlaceSelectionListener());
        if (!start.equals("Chosen address")){
            Place p = Place.builder().setAddress(start).build();
            autocompleteInput.setText(start);
            addressStartingInput = p.getAddress();
        }

        autocompleteEndInput = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment_endLoc);
        autocompleteEndInput.setPlaceFields(Arrays.asList(Place.Field.NAME, Place.Field.ADDRESS));
        autocompleteEndInput.setHint("End Location");
        autocompleteEndInput.setTypeFilter(TypeFilter.ADDRESS);
        autocompleteEndInput.setCountry("RS");
        autocompleteEndInput.setOnPlaceSelectedListener(new PlaceSelectionEndListener());

        Button btnCancel =  findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);
            }
        });

        Button btnConfirm =  findViewById(R.id.btnConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), NewTripActivity.class);

                int selectedRadioButton = radioGroup.getCheckedRadioButtonId();

                if (validateForm()){
                    String[] startTokens = addressStartingInput.split(", ");
                    String[] endTokens = addressEndingInput.split(", ");
                    String cityOne = startTokens[1].replaceAll("\\d", "");
                    String cityTwo = endTokens[1].replaceAll("\\d", "");
                    intent.putExtra("com.example.firstapp.START_LOC", startTokens[0] + ", "+ cityOne);
                    intent.putExtra("com.example.firstapp.END_LOC",endTokens[0] + ", "+ cityTwo);
                    intent.putExtra("com.example.firstapp.NUM_OF_PSG", numOfPsg.getText().toString());

                    RadioButton carType = findViewById(selectedRadioButton);

                    Trip trip = new Trip();
                    trip.setCustomer(customer);
                    Location end = getLocationFromAddress(addressEndingInput);
                    if (end != null) {
                        trip.setEndLocation(end);
                    }
                    Location start = getLocationFromAddress(addressStartingInput);
                    if (start != null) {
                        trip.setStartLocation(start);
                    }
                    trip.setDate(System.currentTimeMillis());
                    trip.setStatus("created");
                    trip.setPassengerNumber(Integer.parseInt(numOfPsg.getText().toString()));
                    if (selectedRadioButton == 0) {
                        trip.setCarType("Car");
                    } else if (selectedRadioButton == 1) {
                        trip.setCarType("Mini-Van");
                    }
                    trip.setPaid(false);
                    Driver driver = getClosestDriver();
                    if (driver != null) {
                        intent.putExtra("com.example.firstapp.TAXI_DRIVER",
                                driver.getLocation().getLatitude() + " " + driver.getLocation().getLongitude());
                        trip.setDriver(driver);
                        intent.putExtra("com.example.firstapp.TAXI_DRIVER_PHONE",
                                driver.getUser().getPhoneNumber());

                        Double price = calculateTripPrice();
                        if (price != null) {
                            intent.putExtra("com.example.firstapp.PRICE", price.toString());
                            trip.setPrice(price);
                        }

                        Log.d("NEW TRIP ", trip.toString());

                        try {
                            String tripID = mDatabaseTrips.push().getKey();
                            mDatabaseTrips.child(tripID).setValue(trip);

                            intent.putExtra("com.example.firstapp.TRIP_ID", tripID);
                        } catch (Exception e) {
                            e.printStackTrace();
                            showToast(e.getMessage());
                        }

                        intent.putExtra("com.example.firstapp.CAR_TYPE", carType.getText().toString());
                        startActivity(intent);

                    } else {
                        showToast("There are no available drivers at the moment. Please try again in a bit.");
                    }
                } else {
                    showToast("Please fill in all necessary fields!");
                }
            }
        });
    }

    public class SingleEventCurrentUserReaderListener implements ValueEventListener {

        public SingleEventCurrentUserReaderListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user.getType().equals("Customer")) {
                customer = user;
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    }

    public class DriverValueListener implements ValueEventListener {

        public DriverValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            availableDrivers.clear();
            for (DataSnapshot ds: dataSnapshot.getChildren()) {
                Driver driver = ds.getValue(Driver.class);
                if (driver.getLocation() != null && driver.isAvailable() &&
                    driver.getCar() != null) {
                    if (driverHasAdditionalOptions(driver)) {
                        availableDrivers.add(driver);
                    }
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            availableDrivers.clear();
            showToast("There are no available drivers at the moment");
            startActivity(new Intent(getApplicationContext(), MapsActivity.class));

        }
    }

    public Driver getClosestDriver() {
        Driver driver = null;
        Double nearest = 0.0;
        if (!availableDrivers.isEmpty()) {
            for (Driver d: availableDrivers) {
                // get nearest driver and choose him
                if (driverHasAdditionalOptions(d)) {
                    Double distance = getDistance(d);
                    if (distance > 0) {
                        if (nearest > 0) {
                            if (nearest > distance) {
                                nearest = distance;
                                driver = d;
                            }
                        } else {
                            nearest = distance;
                            driver = d;
                        }
                    }
                }
            }
        }

        return driver;
    }

    public boolean driverHasAdditionalOptions(Driver driver) {
        if (optionEnglishSpeaking.isChecked() || optionPetFriendly.isChecked() || optionBabySeat.isChecked()){
            if (optionEnglishSpeaking.isChecked() && !driver.getCar().getAdditionalOptions().contains("english-speaking")){
                return false;
            } else if (optionPetFriendly.isChecked() && !driver.getCar().getAdditionalOptions().contains("pet-friendly")) {
                return false;
            } else if (optionBabySeat.isChecked() && !driver.getCar().getAdditionalOptions().contains("baby-seat")) {
                return false;
            }
        }
        int selectedRadioButton = radioGroup.getCheckedRadioButtonId();
        if (selectedRadioButton == 0) {
            if (!driver.getCar().getType().equals("Car")) {
                return false;
            }
        } else if (selectedRadioButton == 1) {
            if (!driver.getCar().getType().equals("Mini-Van")) {
                return false;
            }
        }

        return true;
    }

    public boolean validateForm() {
        if (!addressStartingInput.equals("") && !addressEndingInput.equals("") &&
            !numOfPsg.getText().toString().equals("") && radioGroup.getCheckedRadioButtonId() != -1 ) {
            return true;
        } else {
            return false;
        }
    }

    private Double getDistance(Driver driver) {
        try {
            DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                    .mode(TravelMode.DRIVING)
                    .origin(new LatLng(driver.getLocation().getLatitude(), driver.getLocation().getLongitude()))
                    .destination(addressEndingInput)
                    .departureTime(DateTime.now())
                    .await();
            String dist = result.routes[0].legs[0].distance.toString().split(" ")[0];
            return Double.parseDouble(dist);
        } catch (ApiException e) {
            showToast(e.getMessage());
        } catch (InterruptedException e) {
            showToast(e.getMessage());
        } catch (IOException e) {
            showToast(e.getMessage());
        }
        return 0.0;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(getString(R.string.directApiKey))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    public void showToast(String message) {
        Toast.makeText(CreatingTripActivity.this, message,
                Toast.LENGTH_SHORT).show();
    }

    public Double calculateTripPrice() {
        try {
            DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                    .mode(TravelMode.DRIVING)
                    .origin(addressStartingInput)
                    .destination(addressEndingInput)
                    .departureTime(DateTime.now())
                    .await();
            String dist = result.routes[0].legs[0].distance.toString().split(" ")[0];
            Integer hour = LocalTime.now().getHourOfDay();
            Integer tarrif = 10;
            if (hour >= 22 && hour <= 3) {
                // night shift
                tarrif = 15;
            }
            Double price = tarrif * Double.parseDouble(dist);
            return price;

        } catch (ApiException e) {
            showToast(e.getMessage());
        } catch (InterruptedException e) {
            showToast(e.getMessage());
        } catch (IOException e) {
            showToast(e.getMessage());
        }
        return null;
    }

    public Location getLocationFromAddress(String address) {
        Geocoder geocoder = new Geocoder(getApplicationContext());

        try {
            List<Address> addresses = geocoder.getFromLocationName(address, 1);
            if (addresses == null) {
                return null;
            }
            String[] adrTokens = address.split(", ");
            Address loc = addresses.get(0);
            Location location = new Location();
            location.setLatitude(loc.getLatitude());
            location.setLongitude(loc.getLongitude());
            com.example.firstapp.model.Address adr = new com.example.firstapp.model.Address();
            String city = adrTokens[1].replaceAll("\\d", "");
            adr.setCity(city);
            adr.setStreet(adrTokens[0]);
            location.setAddress(adr);
            return location;

        } catch (IOException e) {
            return null;
        }
    }

    public class PlaceSelectionListener implements com.google.android.libraries.places.widget.listener.PlaceSelectionListener {

        public PlaceSelectionListener() {}

        @Override
        public void onPlaceSelected(@NonNull Place place) {
            String msg = "Place: " + place.getName() + ", " + place.getAddress();
            addressStartingInput = place.getAddress();
            //showToast(msg);
        }

        @Override
        public void onError(@NonNull Status status) {
            addressStartingInput = "";
            showToast("An error occurred.");
        }
    }

    public class PlaceSelectionEndListener implements com.google.android.libraries.places.widget.listener.PlaceSelectionListener {

        public PlaceSelectionEndListener() {}

        @Override
        public void onPlaceSelected(@NonNull Place place) {
            String msg = "Place: " + place.getName() + ", " + place.getAddress();
            addressEndingInput = place.getAddress();
            // showToast(msg);
        }

        @Override
        public void onError(@NonNull Status status) {
            addressEndingInput = "";
            showToast("An error occurred." );
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }
}
