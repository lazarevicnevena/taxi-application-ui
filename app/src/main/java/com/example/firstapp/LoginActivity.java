package com.example.firstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    private DatabaseReference mDatabaseUser;

    private EditText emailText;
    private EditText pwdText;

    private Intent successfulLoginIntent;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        successfulLoginIntent = new Intent(getApplicationContext(), MapsActivity.class);

        mAuth = FirebaseAuth.getInstance();
        emailText = findViewById(R.id.emailLogin);
        pwdText = findViewById(R.id.passwordLogin);

        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users");

    }

    @Override
    protected void onStart(){
        super.onStart();


        Button btnLogin =  findViewById(R.id.loginButton);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateForm()){
                    mAuth.signInWithEmailAndPassword(emailText.getText().toString(), pwdText.getText().toString())
                            .addOnCompleteListener(new LoginListener());
                }
            }
        });

        TextView proceedToReg =  findViewById(R.id.proceedToRegistration);
        proceedToReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.putExtra("com.example.firstapp.REGISTRATION", "customer");
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    public boolean validateForm() {
        if (!emailText.getText().toString().equals("") && !pwdText.getText().toString().equals("") ){
            return true;
        } else {
            Toast.makeText(LoginActivity.this, "Credentials are not valid!",
                    Toast.LENGTH_SHORT).show();
            return false;
        }
    }

    public class LoginListener implements OnCompleteListener<AuthResult> {

        public LoginListener(){}

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()){
                Log.d("Email & Password","signInWithEmail:success");
                if (task.getResult().getUser() != null) {
                    String uuid = task.getResult().getUser().getUid();
                    mDatabaseUser = mDatabaseUser.child(uuid);
                    mDatabaseUser.addListenerForSingleValueEvent(new SingleEventReaderCurrentUserListener());
                } else {
                    Toast.makeText(LoginActivity.this, "Result user is null",
                            Toast.LENGTH_SHORT).show();
                }
            } else {
                Log.d("Email & Password","signInWithEmail:failure");
                Toast.makeText(LoginActivity.this, "Authentication failed",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    public class SingleEventReaderCurrentUserListener implements ValueEventListener {

        public SingleEventReaderCurrentUserListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);
            if (user != null && user.getType() != null) {
                if (user.getType().equals("Customer")) {
                    startActivity(successfulLoginIntent);
                } else if (user.getType().equals("Driver")) {
                    startActivity(new Intent(getApplicationContext(), DriverHomeActivity.class));
                } else if (user.getType().equals("Admin")) {
                    startActivity(new Intent(getApplicationContext(), AdminHomeActivity.class));
                } else {
                    Toast.makeText(getApplicationContext(), "Please try again to login", Toast.LENGTH_SHORT).show();
                    mAuth.signOut();
                }
            } else {
                mAuth.signOut();
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }


}
