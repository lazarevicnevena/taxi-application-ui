package com.example.firstapp;

import android.content.Intent;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.Notification;
import com.example.firstapp.model.Trip;
import com.example.firstapp.model.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.functions.FirebaseFunctions;
import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.android.PolyUtil;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TravelMode;

import org.joda.time.DateTime;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NewTripActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private String userStartAddress = "";
    private LatLng driverAddress;
    private Button payButton;
    private ImageView flagPaid;
    private boolean flagZoom = true;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseUser;
    private DatabaseReference mDatabaseTrip;
    private DatabaseReference mDatabaseDriver;

    private FirebaseFunctions mFunctions;

    private User user;
    private String tripId = "";
    private Double tripPrice;
    private Trip trip;
    private String endAdd;
    private String driverPhone;
    private Driver driver;
    private String driverToken = "";

    private boolean driverNotifiedFlag = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_trip);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapNewTrip);
        mapFragment.getMapAsync(this);

        payButton = findViewById(R.id.btnPay);
        flagPaid = findViewById(R.id.flagPaid);

        initializeTextFields();

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        mDatabaseUser = mDatabase.child("Users").child(mAuth.getCurrentUser().getUid());
        mDatabaseUser.addListenerForSingleValueEvent(new SingleEventReaderCurrentUserListener());

        mDatabaseTrip = mDatabase.child("Trips").child(tripId);
        mDatabaseTrip.addValueEventListener(new TripValueListener());

        driverNotifiedFlag = false;

        mDatabaseDriver = mDatabase.child("Drivers").child(driverPhone);
        mDatabaseDriver.addValueEventListener(new DriverValueListener());
    }


    @Override
    protected void onStart(){
        super.onStart();

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null && tripPrice != null && trip != null) {
                    if (!trip.getPaid()) {
                        if (user.getBalance() > tripPrice) {
                            Double newBalance = user.getBalance() - tripPrice;
                            mDatabaseUser.child("balance").setValue(newBalance);
                            mDatabaseTrip.child("paid").setValue(true);
                            payButton.setVisibility(View.GONE);
                            flagPaid.setVisibility(View.VISIBLE);
                        } else {
                            showToast("You do not have enough money to pay now. Please pay in taxi!");
                        }
                    } else {
                        showToast("You already payed for this trip!");
                    }
                }
            }
        });

        Button buttonCancel = findViewById(R.id.btnCancelNewTrip);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(intent);
            }
        });
    }



    public void initializeTextFields() {
        TextView startLoc = findViewById(R.id.startLocNewTrip);
        TextView endLoc = findViewById(R.id.endLocNewTrip);
        TextView numOfPsg = findViewById(R.id.numOfPsg);
        TextView carType = findViewById(R.id.typeOfCar);
        TextView amount = findViewById(R.id.amount);

        if (getIntent().hasExtra("com.example.firstapp.START_LOC")){
            String stAdd = getIntent().getStringExtra("com.example.firstapp.START_LOC");
            startLoc.setText(stAdd);
            userStartAddress = stAdd;

            endAdd = getIntent().getStringExtra("com.example.firstapp.END_LOC");
            endLoc.setText(endAdd);

            String txtPsg = getIntent().getStringExtra("com.example.firstapp.NUM_OF_PSG");
            numOfPsg.setText(txtPsg);

            String car = getIntent().getStringExtra("com.example.firstapp.CAR_TYPE");
            carType.setText(car);

            if (getIntent().hasExtra("com.example.firstapp.PRICE")) {
                String price = getIntent().getStringExtra("com.example.firstapp.PRICE");
                tripPrice = Double.parseDouble(price);
                amount.setText(price + " $");
            }

            if (getIntent().hasExtra("com.example.firstapp.TAXI_DRIVER")) {
                String[] latlon = getIntent().getStringExtra("com.example.firstapp.TAXI_DRIVER").split(" ");
                Double latitude = Double.parseDouble(latlon[0]);
                Double longitude = Double.parseDouble(latlon[1]);
                driverAddress = new LatLng(latitude, longitude);
                driverPhone = getIntent().getStringExtra("com.example.firstapp.TAXI_DRIVER_PHONE");
            }
            if (getIntent().hasExtra("com.example.firstapp.TRIP_ID")) {
                tripId = getIntent().getStringExtra("com.example.firstapp.TRIP_ID");
            }

            startLoc.setText(stAdd);
            endLoc.setText(endAdd);

        }
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        drawRoute(mMap);

    }

    public void drawRoute(GoogleMap mMap){

        DateTime now = new DateTime();
        try {
            DirectionsResult result = DirectionsApi.newRequest(getGeoContext())
                    .mode(TravelMode.DRIVING)
                    .origin(new com.google.maps.model.LatLng(driverAddress.latitude, driverAddress.longitude))
                    .destination(userStartAddress)
                    .departureTime(now)
                    .await();

            addMarkersToMap(result, mMap);

            addPolyline(result, mMap);


        } catch (ApiException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void addMarkersToMap(DirectionsResult results, GoogleMap mMap) {
        LatLng latLng = getLatLngFromAddress(endAdd);
        if (latLng != null) {
            mMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.finish_flag))
                    .title(endAdd));
        }
        Marker markerStart = mMap.addMarker(new MarkerOptions()
                .position(new LatLng(
                        results.routes[0].legs[0].startLocation.lat,
                        results.routes[0].legs[0].startLocation.lng))
                .icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.gps_marker))
                .title(getEndLocationTitle(results)));
        Marker markerEnd =mMap.addMarker(new MarkerOptions()
                .position(new LatLng(
                        results.routes[0].legs[0].endLocation.lat,
                        results.routes[0].legs[0].endLocation.lng))
                .icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.user_location))
                .title(userStartAddress));

        markerStart.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(results.routes[0].legs[0].startLocation.lat, results.routes[0].legs[0].startLocation.lng)));
    }

    private void addPolyline(DirectionsResult results, GoogleMap mMap) {
        List<LatLng> decodedPath = PolyUtil.decode(results.routes[0]
                .overviewPolyline.getEncodedPath());
        mMap.addPolyline(new PolylineOptions().addAll(decodedPath).color(Color.RED).width(10));

        if (flagZoom) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
            flagZoom = false;
        }
    }

    private String getEndLocationTitle(DirectionsResult results){
        return  "Arriving in "+ results.routes[0].legs[0].duration.humanReadable;
    }

    private GeoApiContext getGeoContext() {
        GeoApiContext geoApiContext = new GeoApiContext();
        return geoApiContext.setQueryRateLimit(3)
                .setApiKey(getString(R.string.directApiKey))
                .setConnectTimeout(1, TimeUnit.SECONDS)
                .setReadTimeout(1, TimeUnit.SECONDS)
                .setWriteTimeout(1, TimeUnit.SECONDS);
    }

    public class SingleEventReaderCurrentUserListener implements ValueEventListener {

        public SingleEventReaderCurrentUserListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class TripValueListener implements ValueEventListener {

        public TripValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            trip = dataSnapshot.getValue(Trip.class);
            if (trip.getPaid()) {
                payButton.setVisibility(View.GONE);
                flagPaid.setVisibility(View.VISIBLE);
            }
            if (trip.getStatus().equals("finished")){
                if (user != null && user.getType() != null && user.getType().equals("Customer")) {
                    showToast("This taxi trip just finished!");
                    Intent intent = new Intent(getApplicationContext(), MapsActivity.class);
                    // startActivity(intent);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class DriverValueListener implements ValueEventListener {

        public DriverValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            driver = dataSnapshot.getValue(Driver.class);
            if (driver.getToken() != null) {
                if (!driver.getToken().equals(driverToken)) {
                    driverToken = driver.getToken();
                }
            }
            if (!driverNotifiedFlag) {
                driverNotifiedFlag = true;
                setDriverUnavailable();
            }
            if (driverAddress.longitude != driver.getLocation().getLongitude()
                    || driverAddress.latitude != driver.getLocation().getLatitude()){
                driverAddress = new LatLng(driver.getLocation().getLatitude(), driver.getLocation().getLongitude());
                mMap.clear();
                drawRoute(mMap);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public void showToast(String message) {
        Toast.makeText(NewTripActivity.this, message,
                Toast.LENGTH_LONG).show();
    }

    public LatLng getLatLngFromAddress(String endAdd) {
        Geocoder geocoder = new Geocoder(getApplicationContext());

        try {
            List<Address> addresses = geocoder.getFromLocationName(endAdd, 1);
            if (addresses == null) {
                return null;
            }
            Address loc = addresses.get(0);
            return new LatLng(loc.getLatitude(), loc.getLongitude());

        }catch (Exception e) {
            return null;
        }
    }

    public void setDriverUnavailable() {
        mDatabaseDriver.child("available").setValue(false);
        if (!tripId.equals("")) {
            mDatabaseDriver.child("currentTrip").setValue(tripId);
        }

        String addressDb = "";
        String tokenDb = "";

        if (userStartAddress != null) {
            addressDb = userStartAddress;
        }
        if (driverToken != null) {
            tokenDb =  driverToken;
        }

        Notification notification = new Notification();
        notification.setAddress(addressDb);
        notification.setToken(tokenDb);
        notification.setTopic("drivers");

        mDatabase.child("Notifications").push().setValue(notification);

    }

}
