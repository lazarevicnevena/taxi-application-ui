package com.example.firstapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.example.firstapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SplashActivity extends AppCompatActivity {

    FirebaseAuth mAuth;

    private DatabaseReference mDatabaseUser;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

        mDatabaseUser = FirebaseDatabase.getInstance().getReference().child("Users");

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser != null) {
            DatabaseReference database = mDatabaseUser.child(currentUser.getUid());
            database.addListenerForSingleValueEvent(new SingleEventReaderCurrentUserListener());
        } else {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }, 2000);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    public class SingleEventReaderCurrentUserListener implements ValueEventListener {

        public SingleEventReaderCurrentUserListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user != null && user.getType() != null) {
                if (user.getType().equals("Customer")) {
                    intent  = new Intent(SplashActivity.this, MapsActivity.class);
                } else if (user.getType().equals("Driver")) {
                    intent = new Intent(SplashActivity.this, DriverHomeActivity.class);
                } else if (user.getType().equals("Admin")) {
                    intent = new Intent(SplashActivity.this, AdminHomeActivity.class);
                } else {
                    intent = new Intent(SplashActivity.this, LoginActivity.class);
                    mAuth.signOut();
                }
            } else {
                intent = new Intent(SplashActivity.this, LoginActivity.class);
                mAuth.signOut();
            }

            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(intent);
                    finish();
                }
            }, 2000);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }
}
