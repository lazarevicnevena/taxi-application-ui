package com.example.firstapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.listeners.MapClickedButtonListener;
import com.example.firstapp.model.Car;
import com.example.firstapp.model.Driver;
import com.example.firstapp.model.Location;
import com.example.firstapp.model.User;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;

    private static final int PICK_FROM_GALLERY = 123;
    public static final int GPS_LOCATION = 321;
    public static final int REQUEST_CHECK_SETTINGS = 111;

    private ImageButton btnLocation;
    private boolean flagZoom = true;

    public static Marker userMarker = null;
    public static MarkerOptions userMarkerOptions = null;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;
    private DatabaseReference mDatabaseUserRef;
    private StorageTask mUploadTask;

    private Uri imageUri;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView avatarView;

    private List<Location> driverLocations = new ArrayList<>();
    private Geocoder geocoder;
    private LocationManager locationManager;
    private FusedLocationProviderClient locationProviderClient;

    private LocationCallback locationCallback;
    private ImageButton navDrawerButton;
    private DrawerLayout drawerLayout;
    private TextView headerToolbar;
    private TextView balanceAccountHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        navDrawerButton = (ImageButton) findViewById(R.id.navDrawer_button);
        navDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
                if(!drawerLayout.isDrawerOpen(GravityCompat.START)){
                    drawerLayout.openDrawer(Gravity.START);
                }else{
                    drawerLayout.closeDrawer(Gravity.END);
                }
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        //initializeDbWithDrivers();

        locationProviderClient = LocationServices.getFusedLocationProviderClient(MapsActivity.this);

        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                android.location.Location location = locationResult.getLastLocation();
                if (location != null) {
                    drawCurrentLocationToMap(new LatLng(location.getLatitude(), location.getLongitude()));
                }

            }
        };


    }

    @Override
    protected void onStart() {
        super.onStart();

        Button btnBook =  findViewById(R.id.btnBook);
        btnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreatingTripActivity.class);
                if (userMarker != null){
                    intent.putExtra("com.example.firstapp.START_POINT", userMarker.getTitle());
                }
                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        } else {
            mStorageRef = FirebaseStorage.getInstance().getReference("avatars");
            mDatabaseRef = FirebaseDatabase.getInstance().getReference("Drivers");
            mDatabaseUserRef = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());

            locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

            userMarker = null;
            userMarkerOptions = null;

            switchToProfileUpdate();

            NavigationView mNavigationView = findViewById(R.id.navigation_view);
            mNavigationView.setNavigationItemSelectedListener(this);
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        geocoder = new Geocoder(this.getApplicationContext(), Locale.getDefault());

        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);


        // user can click anywhere on map and we want to pin that place
        MapClickedButtonListener listener = new MapClickedButtonListener(
                geocoder,
                getApplicationContext(),
                mMap);

        mMap.setOnMapClickListener(listener);

        btnLocation = findViewById(R.id.myLocationBtn);
        btnLocation.setOnClickListener(new myLocationButtonListener());

        // retrieve drivers from DB
        mDatabaseRef.addValueEventListener(new DriverValueListener());
        mDatabaseUserRef.addValueEventListener(new UserValueListener());
        pictureLongPress();

    }

    public void initializeDbWithDrivers() {
        User user = new User();
        user.setBalance(200.0);
        user.setType("Driver");
        user.setGender("Male");
        user.setPhoneNumber("+38164548662");
        user.setFirstName("Miroslav");
        user.setLastName("Miric");
        user.setEmail("mile@gmail.com");
        Car car = new Car();
        car.setRegistryMark("AX102");
        car.setTaxiNumber("103i92");
        car.setType("Car");
        car.setAdditionalOptions(Arrays.asList("baby-seat", "english-speaking"));
        car.setYear(2012);
        com.example.firstapp.model.Address address = new com.example.firstapp.model.Address();
        address.setCity("Novi Sad");
        address.setPostalCode(21000);
        address.setStreet("Radnicka 22");
        Location location = new Location();
        location.setAddress(address);
        location.setLatitude(45.250172);
        location.setLongitude(19.851247);
        Driver driver = new Driver();
        driver.setCar(car);
        driver.setUser(user);
        driver.setAvailable(true);
        driver.setLocation(location);

        User user2 = new User();
        user2.setBalance(0.0);
        user2.setType("Driver");
        user2.setGender("Male");
        user2.setPhoneNumber("+381645434343");
        user2.setFirstName("Anastasije");
        user2.setLastName("Anic");
        user2.setEmail("anastasije@gmail.com");
        Car car2 = new Car();
        car2.setRegistryMark("929-SS");
        car2.setTaxiNumber("AK203-2");
        car2.setType("Mini-Van");
        car2.setAdditionalOptions(Arrays.asList("pet-friendly", "english-speaking"));
        car2.setYear(2016);
        com.example.firstapp.model.Address address2 = new com.example.firstapp.model.Address();
        address2.setCity("Novi Sad");
        address2.setPostalCode(21000);
        address2.setStreet("Alekse Santica 48");
        Location location2 = new Location();
        location2.setAddress(address2);
        location2.setLatitude(45.243351);
        location2.setLongitude(19.834327);
        Driver driver2 = new Driver();
        driver2.setCar(car2);
        driver2.setUser(user2);
        driver2.setAvailable(true);
        driver2.setLocation(location2);

        mDatabaseRef.child("+38164548662").setValue(driver);
        mDatabaseRef.child("+381645434343").setValue(driver2);
    }

    public void switchToProfileUpdate() {
        NavigationView navigationView = findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);
        headerToolbar = headerView.findViewById(R.id.loggedInUsername);
        balanceAccountHeader = headerView.findViewById(R.id.balanceAccount);
        balanceAccountHeader.setVisibility(View.VISIBLE);
        headerToolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.putExtra("com.example.firstapp.UPDATE", "customer");
                startActivity(intent);
            }
        });
    }

    public void pictureLongPress() {
        NavigationView navigationView = findViewById(R.id.navigation_view);
        View headerView = navigationView.getHeaderView(0);
        avatarView = headerView.findViewById(R.id.userAvatar);
        avatarView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                openImageChooser();
                return false;
            }
        });

        if (currentUser != null) {
            String picName = currentUser.getUid() + ".png";
            try {
                mStorageRef.child(picName).getDownloadUrl()
                        .addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    loadImageToImageView(task.getResult());
                                } else {
                                    Log.d("PICTURE ERROR", "Could not find user's picture!");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.d("PICTURE ERROR", "Could not find user's picture!");
            }
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
                } else {
                    Toast.makeText(getApplicationContext(), "You did not allow permission to access gallery",
                            Toast.LENGTH_LONG).show();
                }
                break;
            case GPS_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkGPS();
                } else {
                    Toast.makeText(getApplicationContext(), "You did not allow permission to access your location",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void setDriversToMap(Geocoder geocoder, GoogleMap mMap){
        LatLng ns = new LatLng(45.25167, 19.83694);

        // When driver in DB change we need to refresh markers on map
        mMap.clear();


        if (!driverLocations.isEmpty()){
            for (Location location: driverLocations){
                LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions marker = new MarkerOptions().position(loc);
                try {
                    List<Address> address = geocoder.getFromLocation(loc.latitude, loc.longitude, 1);
                    String street = address.get(0).getAddressLine(0).split(",")[0];
                    marker.title(street);
                }catch (IOException e) {
                    e.printStackTrace();
                }

                marker.icon(bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.gps_marker));
                mMap.addMarker(marker);
            }
        }
        // because we removed all markers from map we have to add user location again
        if (userMarker != null && userMarkerOptions != null) {
            userMarker = mMap.addMarker(userMarkerOptions);
            userMarker.showInfoWindow();
        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(ns));
        }
        if (flagZoom) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
            flagZoom = false;
        }
    }

    public static BitmapDescriptor bitmapDescriptorFromVector(Context context, int vectorResId) {
        // for drawing specific pictures instead of custom marker
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        vectorDrawable.draw(canvas);
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.your_trips){
            Intent intent = new Intent(getApplicationContext(), ListOfRidesActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.sign_out) {
            mAuth.signOut();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.settings) {
            startActivity(new Intent(MapsActivity.this, SettingsActivity.class));
            return true;
        }
       return false;
    }

    public void openImageChooser() {
        // Check for permission
        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
        } else {
            // permission granted!
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
        }

    }

    public void uploadImage() {
        if (imageUri != null) {
            String uid = mAuth.getCurrentUser().getUid();
            final StorageReference storageReference = mStorageRef.child(uid + ".png");
            mUploadTask = storageReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            loadImageToImageView(imageUri);
                            Toast.makeText(MapsActivity.this, "Upload success", Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(MapsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());

                        }
                    });

        }
    }

    public void loadImageToImageView(Uri uri) {

        Picasso.get()
                .load(uri.toString())
                .fit()
                .centerCrop()
                .into(avatarView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();
            if (mUploadTask != null && mUploadTask.isInProgress()) {
                Toast.makeText(MapsActivity.this, "One upload of picture already in progress",
                        Toast.LENGTH_LONG).show();
            } else {
                uploadImage();
            }
        } else if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK ) {
            checkGPS();
        }
    }

    public class DriverValueListener implements ValueEventListener {

        public DriverValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            driverLocations.clear();
            for (DataSnapshot ds: dataSnapshot.getChildren()) {
                Driver driver = ds.getValue(Driver.class);
                if (driver.getLocation() != null) {
                    driverLocations.add(driver.getLocation());
                }
            }
            setDriversToMap(geocoder, mMap);
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
            Toast.makeText(MapsActivity.this, "There are no available drivers at the moment",
                    Toast.LENGTH_LONG).show();

        }
    }

    public class UserValueListener implements ValueEventListener {

        public UserValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user != null) {
                if (user.getFirstName() != null && user.getLastName() != null) {
                    String name = user.getFirstName() + " " + user.getLastName();
                    headerToolbar.setText(name);
                }
                if (user.getBalance() != null) {
                    String balance = user.getBalance().toString() + " $";
                    balanceAccountHeader.setText(balance);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    }

    public class myLocationButtonListener implements View.OnClickListener {

        public myLocationButtonListener() {}

        @Override
        public void onClick(View v) {
            if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MapsActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, MapsActivity.GPS_LOCATION);
            } else {
                checkGPS();
            }
        }
    }

    public void checkGPS(){
        final LocationRequest request = LocationRequest.create();
        request.setInterval(5000);
        request.setFastestInterval(2500);
        request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(request);
        builder.setAlwaysShow(true);

        final SettingsClient client = LocationServices.getSettingsClient(this);
        client.checkLocationSettings(builder.build())
                .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            locationProviderClient.requestLocationUpdates(request,
                                    locationCallback, null);
                        }
                    })
                .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof ResolvableApiException) {
                                try {
                                    // Show the dialog by calling startResolutionForResult(),
                                    // and check the result in onActivityResult().
                                    ResolvableApiException resolvable = (ResolvableApiException) e;
                                    resolvable.startResolutionForResult(MapsActivity.this,
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sendEx) { }
                            }
                        }
                });



    }

    public void drawCurrentLocationToMap(LatLng latLng) {
        if (MapsActivity.userMarker != null) {
            MapsActivity.userMarker.remove();
            MapsActivity.userMarker = null;
        }

        LatLng position = new LatLng(latLng.latitude, latLng.longitude);
        MarkerOptions marker = new MarkerOptions().position(position);
        try {
            List<Address> address = geocoder.getFromLocation(position.latitude, position.longitude, 1);

            String street = address.get(0).getAddressLine(0);
            marker.title(street.split(",")[0] + ", "+ street.split(",")[1]);
        } catch (IOException e) {
            e.printStackTrace();
        }
        marker.icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.user_location));
        MapsActivity.userMarker = mMap.addMarker(marker);
        MapsActivity.userMarker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        stopLocationUpdates();
    }

    private void stopLocationUpdates() {
        locationProviderClient.removeLocationUpdates(locationCallback);
    }

}
