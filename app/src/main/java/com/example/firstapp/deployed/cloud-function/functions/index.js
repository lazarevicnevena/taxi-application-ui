const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


// in order to deploy call: firebase deploy --only functions

exports.databaseTriggering = functions.database.ref('Notifications/{notifId}')
	.onCreate((snapshot, context) => {
		
		const notId = context.params.notifId;
		
		const notification = snapshot.val();
		  
	    // Now grab topic, token and address
		const topic = notification.topic;
		const address = notification.address;
		const token = notification.token;
		
		const bodyText = 'You are assigned to a new trip. Start address: ' + address;
		  
		const payload = {
			notification: {
				title: 'New trip!',
				body: bodyText
			}
		};
		if (token !== "") {
			admin.messaging().sendToDevice(token, payload);
		} else {
			admin.messaging().sendToTopic(topic, payload);
		}
		 
});
