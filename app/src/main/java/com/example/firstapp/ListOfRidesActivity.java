package com.example.firstapp;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.adapters.AdapterForTrips;
import com.example.firstapp.model.Trip;
import com.example.firstapp.viewHolders.TripViewHolder;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListOfRidesActivity extends AppCompatActivity {

    private String currentUserMail;
    private String currentUserUid;
    private TextView numOfRides;
    private FirebaseAuth mAuth;
    private RecyclerView listOfRides;
    private DatabaseReference mDatabase;
    private ArrayList<Trip> trips = new ArrayList<Trip>();
    private ArrayList<String> keys = new ArrayList<String>();

    private ImageView avatarImage;
    private StorageReference storageReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_of_rides);

        mAuth = FirebaseAuth.getInstance();
        currentUserMail = mAuth.getCurrentUser().getEmail();
        currentUserUid = mAuth.getCurrentUser().getUid();

        listOfRides = (RecyclerView) findViewById(R.id.userRides);
        numOfRides = (TextView) findViewById(R.id.numOfTrips);
        avatarImage = (ImageView)findViewById(R.id.avatarImage);

        storageReference = FirebaseStorage.getInstance().getReference().child("avatars");
        storageReference.child(currentUserUid + ".png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                loadImageToImageView(uri, avatarImage);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Toast.makeText(ListOfRidesActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference().child("Trips");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Iterable<DataSnapshot> childern = dataSnapshot.getChildren();

                for (DataSnapshot child: childern) {
                    Trip trip = child.getValue(Trip.class);
                    if(trip.getDateFinished() == null){
                        trip.setDateFinished(1L);
                    }
                    if(trip.getCustomer().getEmail() != null){
                        if(trip.getCustomer().getEmail().equals(currentUserMail)) {
                            trips.add(trip);
                            keys.add(child.getKey().toString());
                        }
                    }

                }
                numOfRides.setText(trips.size() + " " + getResources().getString(R.string.items));
                init();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    public void init(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        listOfRides.setLayoutManager(layoutManager);
        AdapterForTrips adapterForTrips = new AdapterForTrips(this, trips, keys);
        listOfRides.setAdapter(adapterForTrips);
    }

    public void loadImageToImageView(Uri uri, ImageView imageView) {

        Picasso.get()
                .load(uri.toString())
                .fit()
                .centerCrop()
                .into(imageView);
    }

}
