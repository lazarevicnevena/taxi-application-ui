package com.example.firstapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.adapters.AdapterForComments;
import com.example.firstapp.model.Comment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class RideViewActivity extends AppCompatActivity {

    private ArrayList<Comment> comments = new ArrayList<Comment>();
    private ArrayList<String> keys = new ArrayList<String>();
    private DatabaseReference mDatabase;
    private Button button;
    private String tripKey;
    AdapterForComments adapter;

    @Override
    protected void
    onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ride_view);
        getIncomingIntent();
        button = (Button) findViewById(R.id.rateBtn);
        mDatabase = FirebaseDatabase.getInstance().getReference().child("Comments");
        mDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                comments.clear();
                keys.clear();
                Iterable<DataSnapshot> childern = dataSnapshot.getChildren();
                for (DataSnapshot child: childern) {
                    Comment comment = child.getValue(Comment.class);
                    if(comment.getTripId().equals(tripKey)){
                        comments.add(comment);
                        keys.add(child.getKey().toString());
                    }
                }
                initRecyclerView();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openActivity();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void openActivity(){
        Intent intent = new Intent(this, RatingActivity.class);
        intent.putExtra("tripKey", tripKey);
        startActivity(intent);
    }

    private void getIncomingIntent(){
        if(getIntent().hasExtra("startLocation") && getIntent().hasExtra("endLocation") && getIntent().hasExtra("startTime")
                && getIntent().hasExtra("startDate")&& getIntent().hasExtra("finishTime")&& getIntent().hasExtra("finishDate")
                && getIntent().hasExtra("carType") && getIntent().hasExtra("price") && getIntent().hasExtra("number")
                && getIntent().hasExtra("tripId") && getIntent().hasExtra("index")){

            String startLocation = getIntent().getStringExtra("startLocation");
            String startDate = getIntent().getStringExtra("startDate");
            String startTime = getIntent().getStringExtra("startTime");
            String finishDate = getIntent().getStringExtra("finishDate");
            String finishTime = getIntent().getStringExtra("finishTime");
            String endLocation = getIntent().getStringExtra("endLocation");
            String carType = getIntent().getStringExtra("carType");
            String number = getIntent().getStringExtra("number");
            String price = getIntent().getStringExtra("price");
            tripKey = getIntent().getStringExtra("tripId");
            int index = (int) getIntent().getIntExtra("index", 0);

            setData(startLocation, startTime, startDate, finishTime, finishDate, endLocation, carType, number, price, tripKey, index);
        }
    }

    private void setData(String startLocation, String startTime, String startDate, String finishTime, String finishDate, String endLocation, String car,
                         String number, String price, String tripId, int index){
        TextView currentLocation = findViewById(R.id.currentLocation);
        currentLocation.setText(startLocation);
        TextView oclockTime = findViewById(R.id.oclockTime);
        oclockTime.setText(startTime);
        TextView date = findViewById(R.id.date);
        date.setText(startDate);
        TextView oclockTime1 = findViewById(R.id.oclockTime1);
        oclockTime1.setText(finishTime);
        TextView date1 = findViewById(R.id.date1);
        date1.setText(finishDate);
        TextView targetLocation = findViewById(R.id.targetLocation);
        targetLocation.setText(endLocation);
        TextView carType = findViewById(R.id.carType);
        carType.setText(car);
        TextView numberOfPassengers = findViewById(R.id.numberOfPassengers);
        numberOfPassengers.setText(number);
        TextView amount = findViewById(R.id.amount);
        amount.setText(price);
        TextView identificator = findViewById(R.id.identificator);
        identificator.setText(getResources().getString(R.string.trip) + index);

    }

    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //layoutManager.setReverseLayout(true);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.comments);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AdapterForComments(this, comments, keys);
        recyclerView.setAdapter(adapter);
    }
}
