package com.example.firstapp.viewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.firstapp.R;

public class CommentViewHolder extends RecyclerView.ViewHolder {

    public TextView userName;
    public TextView userComment;
    public TextView commentTime;
    public RatingBar mark;
    public TextView deleteComment;
    public ImageView personImage;

    public CommentViewHolder(View itemView) {
        super(itemView);

        userName = itemView.findViewById(R.id.personName);
        userComment = itemView.findViewById(R.id.commentsContent);
        commentTime = itemView.findViewById(R.id.commentTime);
        mark = itemView.findViewById(R.id.rating);
        deleteComment = (TextView) itemView.findViewById(R.id.deleteComment);
        personImage = (ImageView) itemView.findViewById(R.id.person);
    }
}
