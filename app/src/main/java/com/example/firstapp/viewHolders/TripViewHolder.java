package com.example.firstapp.viewHolders;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.firstapp.R;

public class TripViewHolder extends RecyclerView.ViewHolder {

    public TextView tripId;
    public TextView startLocation;
    public TextView startTime;
    public TextView targetDestination;
    public TextView endTime;
    public TextView seeMore;
    public TextView price;

    public TripViewHolder(View itemView) {
        super(itemView);

        startLocation = itemView.findViewById(R.id.startLocation);
        startTime = itemView.findViewById(R.id.startTime);
        targetDestination = itemView.findViewById(R.id.targetDestination);
        endTime = itemView.findViewById(R.id.endTime);
        seeMore = itemView.findViewById(R.id.seeMore);
        price = itemView.findViewById(R.id.price);
        tripId = itemView.findViewById(R.id.tripId);
    }

}
