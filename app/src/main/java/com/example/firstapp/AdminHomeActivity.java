package com.example.firstapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.Location;
import com.example.firstapp.model.User;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class AdminHomeActivity extends FragmentActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;
    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabaseUserRef;
    private DatabaseReference mDatabaseDriverRef;
    private StorageReference mStorageRef;
    private StorageTask mUploadTask;

    private static final int PICK_FROM_GALLERY = 123;
    private Uri imageUri;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView avatarView;

    private ImageButton navDrawerButton;
    private DrawerLayout drawerLayout;

    private List<Location> driverLocations = new ArrayList<>();
    private Geocoder geocoder;

    private TextView headerToolbar;
    private boolean flagZoom = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_home);

        navDrawerButton = findViewById(R.id.navDrawer_buttonAdmin);
        navDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout = findViewById(R.id.drawer_layoutAdmin);
                if(!drawerLayout.isDrawerOpen(GravityCompat.START)){
                    drawerLayout.openDrawer(Gravity.START);
                }else{
                    drawerLayout.closeDrawer(Gravity.END);
                }
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapAdmin);
        mapFragment.getMapAsync(this);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Button btnRegister =  findViewById(R.id.btnAddDriver);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                intent.putExtra("com.example.firstapp.REGISTRATION", "driver");
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        if (currentUser == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        } else {
            mDatabaseUserRef = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());
            mStorageRef = FirebaseStorage.getInstance().getReference("avatars");
            mDatabaseDriverRef = FirebaseDatabase.getInstance().getReference("Drivers");

            NavigationView mNavigationView = findViewById(R.id.navigation_viewAdmin);
            (mNavigationView.getMenu().getItem(0)).setVisible(false);
            (mNavigationView.getMenu().getItem(2)).setVisible(false);
            mNavigationView.setNavigationItemSelectedListener(this);

            View headerView = mNavigationView.getHeaderView(0);
            headerToolbar = headerView.findViewById(R.id.loggedInUsername);

            headerToolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    intent.putExtra("com.example.firstapp.UPDATE", "admin");
                    startActivity(intent);
                }
            });
        }


    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        geocoder = new Geocoder(this.getApplicationContext(), Locale.getDefault());

        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        LatLng ns = new LatLng(45.25167, 19.83694);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ns));

        // retrieve drivers from DB
        mDatabaseDriverRef.addValueEventListener(new DriverValueListener());
        mDatabaseUserRef.addValueEventListener(new UserValueListener());

        pictureLongPress();
    }

    public class DriverValueListener implements ValueEventListener {

        public DriverValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            driverLocations.clear();
            for (DataSnapshot ds: dataSnapshot.getChildren()) {
                Driver driver = ds.getValue(Driver.class);
                if (driver != null) {
                    if (driver.getLocation() != null) {
                        driverLocations.add(driver.getLocation());
                    }
                }
                setDriversToMap(geocoder, mMap);
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    }

    public class UserValueListener implements ValueEventListener {

        public UserValueListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user != null) {
                if (user.getFirstName() != null && user.getLastName() != null) {
                    String name = user.getFirstName() + " " + user.getLastName();
                    headerToolbar.setText(name);
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    }

    public void setDriversToMap(Geocoder geocoder, GoogleMap mMap){

        // When driver in DB change we need to refresh markers on map
        mMap.clear();


        if (!driverLocations.isEmpty()){
            for (Location location: driverLocations){
                LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions marker = new MarkerOptions().position(loc);
                try {
                    List<Address> address = geocoder.getFromLocation(loc.latitude, loc.longitude, 1);
                    String street = address.get(0).getAddressLine(0).split(",")[0];
                    marker.title(street);
                }catch (IOException e) {
                    e.printStackTrace();
                }

                marker.icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.gps_marker));
                mMap.addMarker(marker);
            }
        }
        if (flagZoom) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
            flagZoom = false;
        }

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sign_out) {
            mAuth.signOut();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }
        return false;
    }

    public void pictureLongPress() {
        NavigationView navigationView = findViewById(R.id.navigation_viewAdmin);
        View headerView = navigationView.getHeaderView(0);
        avatarView = headerView.findViewById(R.id.userAvatar);
        avatarView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                openImageChooser();
                return false;
            }
        });

        if (currentUser != null) {
            String picName = currentUser.getUid() + ".png";
            try {
                mStorageRef.child(picName).getDownloadUrl()
                        .addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    loadImageToImageView(task.getResult());
                                } else {
                                    Log.d("PICTURE ERROR", "Could not find user's picture!");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.d("PICTURE ERROR", "Could not find user's picture!");
            }
        }


    }

    public void openImageChooser() {
        // Check for permission
        if (ActivityCompat.checkSelfPermission(AdminHomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(AdminHomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
        } else {
            // permission granted!
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
        }

    }

    public void uploadImage() {
        if (imageUri != null) {
            String uid = mAuth.getCurrentUser().getUid();
            final StorageReference storageReference = mStorageRef.child(uid + ".png");
            mUploadTask = storageReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            loadImageToImageView(imageUri);
                            Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());

                        }
                    });

        }
    }

    public void loadImageToImageView(Uri uri) {

        Picasso.get()
                .load(uri.toString())
                .fit()
                .centerCrop()
                .into(avatarView);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();
            if (mUploadTask != null && mUploadTask.isInProgress()) {
                Toast.makeText(getApplicationContext(), "One upload of picture already in progress",
                        Toast.LENGTH_LONG).show();
            } else {
                uploadImage();
            }
        }
    }
}
