package com.example.firstapp.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Driver {
    private Location location;
    private User user;
    private Car car;
    private boolean available;
    private String currentTrip;
    private String token;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("location", location);
        result.put("user", user);
        result.put("car", car);
        result.put("available", available);
        result.put("currentTrip", currentTrip);
        result.put("token", token);
        return result;
    }
}
