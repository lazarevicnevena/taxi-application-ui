package com.example.firstapp.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Trip {
    private Long date;
    private Location startLocation;
    private String carType;
    private Integer passengerNumber;
    private User customer;
    private Location endLocation;
    private Driver driver;
    private Double price;
    private Comment comment;
    private String status;
    private Boolean paid;
    private Long dateFinished;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("date", date);
        result.put("startLocation", startLocation);
        result.put("customer", customer);
        result.put("carType", carType);
        result.put("passengerNumber", passengerNumber);
        result.put("endLocation", endLocation);
        result.put("driver", driver);
        result.put("price", price);
        result.put("comment", comment);
        result.put("status", status);
        result.put("paid", paid);
        result.put("dateFinished", dateFinished);
        return result;
    }
}
