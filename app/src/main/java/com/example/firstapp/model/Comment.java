package com.example.firstapp.model;

import com.google.firebase.database.Exclude;

import org.joda.time.LocalDateTime;

import java.util.HashMap;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Comment {
    private String description;
    private Long date;
    private User user;
    private String tripId;
    private float rate;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("description", description);
        result.put("date", date);
        result.put("user", user);
        result.put("tripId", tripId);
        result.put("rate", rate);
        return result;
    }
}
