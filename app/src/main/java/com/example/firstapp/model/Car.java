package com.example.firstapp.model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Car {
    private Integer year;
    private String registryMark;
    private String taxiNumber;
    private String type; // Car / Mini-Van
    private List<String> additionalOptions = new ArrayList<>();

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("year", year);
        result.put("registryMark", registryMark);
        result.put("taxiNumber", taxiNumber);
        result.put("type", type);
        result.put("additionalOptions", additionalOptions);
        return result;
    }
}
