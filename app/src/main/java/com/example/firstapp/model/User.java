package com.example.firstapp.model;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class User {
    private String firstName;
    private String lastName;
    private String phoneNumber;
    private String email;
    private String gender;
    private String type; // Customer, Driver, Admin
    private Double balance;

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("firstName", firstName);
        result.put("lastName", lastName);
        result.put("phoneNumber", phoneNumber);
        result.put("email", email);
        result.put("gender", gender);
        result.put("type", type);
        result.put("balance", balance);
        return result;
    }
}
