package com.example.firstapp;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.tooltip.Tooltip;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final String TAG = "RecyclerViewAdapter";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private Context mContext;

    public RecyclerViewAdapter(Context context, ArrayList<String> names, ArrayList<String> imageUrls){
        mNames = names;
        mImageUrls = imageUrls;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Log.d(TAG, "onBindViewHolder: called.");

        holder.image.setImageResource(Integer.valueOf(mImageUrls.get(position)));
        holder.name.setText(mNames.get(position));
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation animation = AnimationUtils.loadAnimation(mContext, R.anim.zoom_in);
                holder.view.startAnimation(animation);
                switch (position){
                    case 0:
                        showTooltip(v, mContext.getResources().getString(R.string.es_impressions), Gravity.BOTTOM);
                        break;
                    case 1:
                        showTooltip(v, mContext.getResources().getString(R.string.gc_impressions), Gravity.TOP);
                        break;
                    case 2:
                        showTooltip(v, mContext.getResources().getString(R.string.en_impressions), Gravity.BOTTOM);
                        break;
                    case 3:
                        showTooltip(v, mContext.getResources().getString(R.string.ed_impressions), Gravity.TOP);
                        break;
                    case 4:
                        showTooltip(v, mContext.getResources().getString(R.string.cc_impressions), Gravity.BOTTOM);
                        break;
                    case 5:
                        showTooltip(v, mContext.getResources().getString(R.string.am_impressions), Gravity.TOP);
                        break;
                    case 6:
                        showTooltip(v, mContext.getResources().getString(R.string.nt_impressions), Gravity.BOTTOM);
                        break;

                }

            }
        });
    }

    public void showTooltip(View view, String text, int gravity){
        final Tooltip tooltip = new Tooltip.Builder(view)
                .setText(text)
                .setTextColor(Color.WHITE)
                .setCornerRadius(8f)
                .setGravity(gravity)
                .show();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                tooltip.dismiss();
            }
        }, 2000);
    }

    @Override
    public int getItemCount() {
        return mImageUrls.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        View view;

        public ViewHolder(View itemView){
            super(itemView);
            image = itemView.findViewById(R.id.image_view);
            name = itemView.findViewById(R.id.name);
            view = itemView;

        }

    }
}
