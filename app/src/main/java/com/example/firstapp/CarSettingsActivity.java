package com.example.firstapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class CarSettingsActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabaseRefUser;
    private DatabaseReference mDatabaseRefDriver;
    private Driver driver;

    private String phoneNumber;

    private Intent intent;

    private EditText year;
    private EditText registryMarks;
    private EditText taxiNumber;
    private Spinner type;
    private CheckBox petFriendly;
    private CheckBox englishSpeaking;
    private CheckBox babySeat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_settings);

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();
        mDatabaseRefUser = FirebaseDatabase.getInstance().getReference().child("Users").child(currentUser.getUid());

    }

    @Override
    protected void onResume() {
        super.onResume();
        year = findViewById(R.id.yearCarSettings);
        registryMarks = findViewById(R.id.registryMarksCarSettings);
        taxiNumber = findViewById(R.id.taxiNumberCarSettings);
        type = findViewById(R.id.spinnerCarSettingsType);
        petFriendly = findViewById(R.id.btnPetFriendlyCarSettings);
        englishSpeaking = findViewById(R.id.btnEnglishSpeakingCarSettings);
        babySeat = findViewById(R.id.btnBabySeatCarSettings);

        intent = new Intent(getApplicationContext(), DriverHomeActivity.class);
        mDatabaseRefUser.addListenerForSingleValueEvent(new UserValueEventListener());
        Button buttonCancel = findViewById(R.id.btnCarSettingsCancel);

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(CarSettingsActivity.this.intent);
            }
        });

        Button buttonConfirm = findViewById(R.id.btnCarSettingsConfirm);
        buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateCar();
                startActivity(CarSettingsActivity.this.intent);
            }
        });


    }

    private void updateCar() {
        if (!year.getText().toString().equals("")) {
            mDatabaseRefDriver.child("car").child("year").setValue(Integer.parseInt(year.getText().toString()));
        } else {
            Toast.makeText(getApplicationContext(), "Year field is required", Toast.LENGTH_SHORT).show();
        }

        if (!registryMarks.getText().toString().equals("")) {
            mDatabaseRefDriver.child("car").child("registryMark").setValue(registryMarks.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), "Registry marks field is required", Toast.LENGTH_SHORT).show();
        }

        if (!taxiNumber.getText().toString().equals("")) {
            mDatabaseRefDriver.child("car").child("taxiNumber").setValue(taxiNumber.getText().toString());
        } else {
            Toast.makeText(getApplicationContext(), "Taxi number field is required", Toast.LENGTH_SHORT).show();
        }

        if (type.getSelectedItemPosition() == 0) {
            mDatabaseRefDriver.child("car").child("type").setValue("Car");
        } else {
            mDatabaseRefDriver.child("car").child("type").setValue("Mini-Van");
        }
        List<String> checks = new ArrayList<>();
        if (petFriendly.isChecked()) {
            checks.add("pet-friendly");
        }
        if (englishSpeaking.isChecked()) {
            checks.add("english-speaking");
        }
        if (babySeat.isChecked()) {
            checks.add("baby-seat");
        }
        mDatabaseRefDriver.child("car").child("additionalOptions").setValue(checks);
    }

    public class UserValueEventListener implements ValueEventListener {

        public UserValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            if (user != null) {
                // subscribing user to topic of their mobile phone number || MAYBE ID OF USER I HAVE TO SEE
                phoneNumber = user.getPhoneNumber();

                mDatabaseRefDriver = FirebaseDatabase.getInstance().getReference("Drivers").child(phoneNumber);
                mDatabaseRefDriver.addListenerForSingleValueEvent(new DriverValueEventListener());
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class DriverValueEventListener implements ValueEventListener {

        public DriverValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            driver = dataSnapshot.getValue(Driver.class);
            if (driver != null) {
                if (driver.getCar() != null) {
                    if (driver.getCar().getYear() != null) {
                        year.setText(driver.getCar().getYear().toString());
                    }
                    if (driver.getCar().getRegistryMark() != null && !driver.getCar().getRegistryMark().equals("")) {
                        registryMarks.setText(driver.getCar().getRegistryMark());
                    }
                    if (driver.getCar().getTaxiNumber() != null && !driver.getCar().getTaxiNumber().equals("")) {
                        taxiNumber.setText(driver.getCar().getTaxiNumber());
                    }
                    if (driver.getCar().getType() != null && !driver.getCar().getType().equals("")) {
                        if (driver.getCar().getType().equals("Car")) {
                            type.setSelection(0);
                        } else if (driver.getCar().getType().equals("Mini-Van")) {
                            type.setSelection(1);
                        }
                    }
                    if (driver.getCar().getAdditionalOptions() != null && !driver.getCar().getAdditionalOptions().isEmpty()) {
                        for (String option: driver.getCar().getAdditionalOptions()) {
                            if (option.equals("pet-friendly")) {
                                petFriendly.setChecked(true);
                            }
                            if (option.equals("english-speaking")) {
                                englishSpeaking.setChecked(true);
                            }
                            if (option.equals("baby-seat")) {
                                babySeat.setChecked(true);
                            }
                        }
                    }
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

}
