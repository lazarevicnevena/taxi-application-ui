package com.example.firstapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.widget.Toast;

import com.example.firstapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Locale;

public class SettingsActivity extends PreferenceActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MainSettingsFragment()).commit();
    }

    public static class MainSettingsFragment extends PreferenceFragment{
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            addPreferencesFromResource(R.xml.preferences);
            findPreference("language_key").setOnPreferenceChangeListener(listener);

            Preference myPref = (Preference) findPreference("change_password");
            myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    changePasswordByUserType(preference);
                    return true;
                }
            });
        }

        private void changePasswordByUserType(final Preference preference){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String userId = user.getUid();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
            reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String userType = dataSnapshot.getValue(User.class).getType();
                    if(userType.equals("Customer")){
                        Intent intent = new Intent(preference.getContext(), RegistrationActivity.class);
                        intent.putExtra("com.example.firstapp.PASSWORD", "customer");
                        preference.getContext().startActivity(intent);
                    }else if(userType.equals("Driver")){
                        Intent intent = new Intent(preference.getContext(), RegistrationActivity.class);
                        intent.putExtra("com.example.firstapp.PASSWORD", "driver");
                        preference.getContext().startActivity(intent);
                    }else{
                        Intent intent = new Intent(preference.getContext(), RegistrationActivity.class);
                        intent.putExtra("com.example.firstapp.PASSWORD", "admin");
                        preference.getContext().startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    }

    private static Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object newValue) {
            if(preference instanceof ListPreference) {
                String stringValue = newValue.toString();
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);
                CharSequence[] entryValues = listPreference.getEntryValues();
                if (index == 0) {
                    setLocal("en", preference);
                } else if (index == 1) {
                    setLocal("sr", preference);
                } else {
                    setLocal("de", preference);
                }
            }
            return true;
        }

        private void setLocal(String lang, Preference preference){
            Locale myLocale = new Locale(lang);
            Resources res = preference.getContext().getResources();
            DisplayMetrics dm = res.getDisplayMetrics();
            Configuration conf = res.getConfiguration();
            conf.locale = myLocale;
            res.updateConfiguration(conf, dm);
            getSettingByUserType(preference);
            //Intent intent = new Intent(preference.getContext(), MapsActivity.class);
            //preference.getContext().startActivity(intent);
        }

        private void getSettingByUserType(final Preference preference){
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            String userId = user.getUid();
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Users");
            reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String userType = dataSnapshot.getValue(User.class).getType();
                    if(userType.equals("Customer")){
                        Intent intent = new Intent(preference.getContext(), MapsActivity.class);
                        preference.getContext().startActivity(intent);
                    }else if(userType.equals("Driver")){
                        Intent intent = new Intent(preference.getContext(), DriverHomeActivity.class);
                        preference.getContext().startActivity(intent);
                    }else{
                        Intent intent = new Intent(preference.getContext(), AdminHomeActivity.class);
                        preference.getContext().startActivity(intent);
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }

    };

}
