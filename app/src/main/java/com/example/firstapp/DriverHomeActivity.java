package com.example.firstapp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.Location;
import com.example.firstapp.model.Trip;
import com.example.firstapp.model.User;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class DriverHomeActivity extends FragmentActivity implements OnMapReadyCallback, NavigationView.OnNavigationItemSelectedListener {

    private GoogleMap mMap;

    public static final int GPS_LOCATION = 321;
    public static final int REQUEST_CHECK_SETTINGS = 111;

    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;
    private DatabaseReference mDatabaseRef;
    private DatabaseReference mDatabaseUser;
    private DatabaseReference mDatabaseTrip;
    private StorageReference mStorageRef;
    private StorageTask mUploadTask;

    private static final int PICK_FROM_GALLERY = 123;
    private Uri imageUri;
    private static final int PICK_IMAGE_REQUEST = 1;
    private ImageView avatarView;

    private Geocoder geocoder;
    private LocationManager locationManager;

    private Driver driver;
    private User user;
    private String tripId = "";
    private Trip trip;

    private NavigationView navigationView;

    private TextView headerToolbar;

    private Marker userMarker = null;
    private Marker userEndMarker = null;
    private Marker driverMarker;
    private boolean flagZoom = true;

    private LocationCallback locationCallback;
    private FusedLocationProviderClient locationProviderClient;
    private boolean locationUpdateFlag;

    private LocationRequest request;

    private ImageButton buttonFinish;
    private Ringtone mp;

    private String phoneNumber;

    private ImageButton navDrawerButton;
    private DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_home);

        navDrawerButton = findViewById(R.id.navDrawer_buttonDriver);
        navDrawerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout = findViewById(R.id.drawer_layoutDriver);
                if(!drawerLayout.isDrawerOpen(GravityCompat.START)){
                    drawerLayout.openDrawer(Gravity.START);
                }else{
                    drawerLayout.closeDrawer(Gravity.END);
                }
            }
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapDriver);
        mapFragment.getMapAsync(this);


        locationManager = (LocationManager)getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                android.location.Location location = locationResult.getLastLocation();
                if (location != null) {
                    // Toast.makeText(getApplicationContext(), "New location update happened", Toast.LENGTH_SHORT).show();
                    Location loc = new Location();
                    loc.setLatitude(location.getLatitude());
                    loc.setLongitude(location.getLongitude());
                    updateLocationInDb(loc);
                    drawDriverToMap(new LatLng(location.getLatitude(), location.getLongitude()));
                }

            }
        };

        locationProviderClient = LocationServices.getFusedLocationProviderClient(DriverHomeActivity.this);


        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mp = RingtoneManager.getRingtone(getApplicationContext(), uri);

    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationUpdateFlag = false;

        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth.getCurrentUser();

        if (currentUser == null) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            finish();
        } else {
            mStorageRef = FirebaseStorage.getInstance().getReference("avatars");

            navigationView = findViewById(R.id.navigation_viewDriver);
            (navigationView.getMenu().getItem(0)).setVisible(false);
            (navigationView.getMenu().getItem(2)).setVisible(true);
            navigationView.setNavigationItemSelectedListener(this);

            View headerView = navigationView.getHeaderView(0);
            headerToolbar = headerView.findViewById(R.id.loggedInUsername);
            headerToolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), RegistrationActivity.class);
                    intent.putExtra("com.example.firstapp.UPDATE", "driver");
                    startActivity(intent);
                }
            });

            // headerView.findViewById(R.id.userAvatar).setVisibility(View.GONE);

            buttonFinish = findViewById(R.id.btnFinishTrip);

            userMarker = null;
            userEndMarker = null;
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        geocoder = new Geocoder(this.getApplicationContext(), Locale.getDefault());

        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        LatLng ns = new LatLng(45.25167, 19.83694);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(ns));

        phoneNumber = "";

        mDatabaseUser = FirebaseDatabase.getInstance().getReference("Users").child(currentUser.getUid());
        mDatabaseUser.addListenerForSingleValueEvent(new UserValueEventListener());
        mDatabaseUser.addValueEventListener(new UserMultipleValueEventListener());

        if (ActivityCompat.checkSelfPermission(DriverHomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DriverHomeActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, MapsActivity.GPS_LOCATION);
        } else {
            startLocationUpdates();
        }

        pictureLongPress();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.sign_out) {
            stopLocationUpdates();
            mAuth.signOut();
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        } else if (id == R.id.car_settings) {
            Intent intent = new Intent(getApplicationContext(), CarSettingsActivity.class);
            startActivity(intent);
        }
        return false;
    }

    public class DriverValueEventListener implements ValueEventListener {

        public DriverValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            driver = dataSnapshot.getValue(Driver.class);
            if (driver != null) {

                // change in trip
                if (!driver.isAvailable()) {
                    if (!locationUpdateFlag) {
                        locationUpdateFlag = true;
                        if (driver.getCurrentTrip() != null) {
                            if (!driver.getCurrentTrip().equals("")) {
                                tripId = driver.getCurrentTrip();
                                mDatabaseTrip = FirebaseDatabase.getInstance().getReference("Trips").child(tripId);
                                mDatabaseTrip.addListenerForSingleValueEvent(new TripValueEventListener());
                            }
                        }
                        buttonFinish.setVisibility(View.VISIBLE);
                        buttonFinish.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finishTrip();
                            }
                        });
                        // mp.play();
                        Toast.makeText(getApplicationContext(), "You are given a new trip!", Toast.LENGTH_LONG).show();

                    }
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class UserValueEventListener implements ValueEventListener {

        public UserValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);
            if (user != null) {
                // subscribing user to topic of their mobile phone number || MAYBE ID OF USER I HAVE TO SEE
                phoneNumber = user.getPhoneNumber();

                FirebaseMessaging.getInstance().subscribeToTopic("drivers");
                mDatabaseRef = FirebaseDatabase.getInstance().getReference("Drivers").child(phoneNumber);
                checkToken();
                mDatabaseRef.addValueEventListener(new DriverValueEventListener());
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class UserMultipleValueEventListener implements ValueEventListener {

        public UserMultipleValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);
            if (user != null) {
               if (user.getFirstName() != null && user.getLastName() != null) {
                   String name = user.getFirstName() + " " + user.getLastName();
                   headerToolbar.setText(name);
               }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    public class TripValueEventListener implements ValueEventListener {

        public TripValueEventListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            trip = dataSnapshot.getValue(Trip.class);
            if (trip != null) {
                if (trip.getStatus() != null && !trip.getStatus().equals("finished")) {
                    drawUserToMap(trip.getStartLocation());
                    drawEndLocationToMap(trip.getEndLocation());
                }
            }
        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK ) {
            startLocationUpdates();
        } else if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            imageUri = data.getData();
            if (mUploadTask != null && mUploadTask.isInProgress()) {
                Toast.makeText(getApplicationContext(), "One upload of picture already in progress",
                        Toast.LENGTH_LONG).show();
            } else {
                uploadImage();
            }
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(DriverHomeActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DriverHomeActivity.this, new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, MapsActivity.GPS_LOCATION);
        } else {
            request = LocationRequest.create();
            request.setInterval(4000);
            request.setFastestInterval(2000);
            request.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(request);
            builder.setAlwaysShow(true);

            SettingsClient client = LocationServices.getSettingsClient(this);
            client.checkLocationSettings(builder.build())
                    .addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
                        @Override
                        public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                            locationProviderClient.requestLocationUpdates(request,
                                    locationCallback, null);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            if (e instanceof ResolvableApiException) {
                                try {
                                    ResolvableApiException resolvable = (ResolvableApiException) e;
                                    resolvable.startResolutionForResult(DriverHomeActivity.this,
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException sendEx) { }
                            }
                        }
                    });

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults)
    {
        switch (requestCode) {
            case GPS_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    Toast.makeText(getApplicationContext(), "You did not allow permission to access your location which is crucial!",
                            Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    public void updateLocationInDb(Location location) {
        // Toast.makeText(getApplicationContext(), "Updating databases location: " + location.getLatitude() + " lon: " + location.getLongitude(), Toast.LENGTH_LONG).show();
        mDatabaseRef.child("location").updateChildren(location.toMap());
    }

    public void drawDriverToMap(LatLng latLng) {
        if (driverMarker != null) {
            driverMarker.remove();
            driverMarker = null;
        }
        MarkerOptions marker = new MarkerOptions().position(latLng);

        try {
            List<Address> address = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

            String street = address.get(0).getAddressLine(0);
            marker.title(street.split(",")[0]);
        } catch (IOException e) {
            e.printStackTrace();
        }

        marker.icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.gps_marker));
        driverMarker = mMap.addMarker(marker);
        // driverMarker.showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        if (flagZoom) {
            mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
            flagZoom = false;
        }
    }

    public void drawUserToMap(Location location){
        if (userMarker != null) {
            userMarker.remove();
            userMarker = null;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        String street = location.getAddress().getStreet() + ", " + location.getAddress().getCity();

        MarkerOptions marker = new MarkerOptions().position(latLng);
        marker.title("Start: " + street);

        marker.icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.user_location));
        userMarker = mMap.addMarker(marker);
        userMarker.showInfoWindow();
    }

    public void drawEndLocationToMap(Location location) {
        if (userEndMarker != null) {
            userEndMarker.remove();
            userEndMarker = null;
        }
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        String street = location.getAddress().getStreet() + ", " + location.getAddress().getCity();

        MarkerOptions marker = new MarkerOptions().position(latLng);
        marker.title("End: " + street);

        marker.icon(MapsActivity.bitmapDescriptorFromVector(getApplicationContext(), R.mipmap.finish_flag));
        userEndMarker = mMap.addMarker(marker);
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
        locationUpdateFlag = false;
    }

    private void stopLocationUpdates() {
        locationProviderClient.removeLocationUpdates(locationCallback);
    }

    public void finishTrip() {
        stopLocationUpdates();
        mDatabaseRef.child("available").setValue(true);
        mDatabaseRef.child("currentTrip").setValue("");
        buttonFinish.setVisibility(View.INVISIBLE);
        userMarker.remove();
        userMarker = null;
        userEndMarker.remove();
        userEndMarker = null;

        // Now we have to make changes in trip in db
        mDatabaseTrip.child("status").setValue("finished");
        mDatabaseTrip.child("paid").setValue(true);
        mDatabaseTrip.child("dateFinished").setValue(System.currentTimeMillis());
    }

    public void checkToken() {
        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
            @Override
            public void onSuccess(InstanceIdResult instanceIdResult) {
                String token = instanceIdResult.getToken();
                mDatabaseRef.child("token").setValue(token);
            }
        });
    }

    public void pictureLongPress() {
        NavigationView navigationView = findViewById(R.id.navigation_viewDriver);
        View headerView = navigationView.getHeaderView(0);
        avatarView = headerView.findViewById(R.id.userAvatar);
        avatarView.setOnLongClickListener(new View.OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                openImageChooser();
                return false;
            }
        });

        if (currentUser != null) {
            String picName = currentUser.getUid() + ".png";
            try {
                mStorageRef.child(picName).getDownloadUrl()
                        .addOnCompleteListener(new OnCompleteListener<Uri>() {
                            @Override
                            public void onComplete(@NonNull Task<Uri> task) {
                                if (task.isSuccessful()) {
                                    loadImageToImageView(task.getResult());
                                } else {
                                    Log.d("PICTURE ERROR", "Could not find user's picture!");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.d("PICTURE ERROR", "Could not find user's picture!");
            }
        }


    }

    public void openImageChooser() {
        // Check for permission
        if (ActivityCompat.checkSelfPermission(DriverHomeActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(DriverHomeActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PICK_FROM_GALLERY);
        } else {
            // permission granted!
            Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(galleryIntent, PICK_IMAGE_REQUEST);
        }

    }

    public void uploadImage() {
        if (imageUri != null) {
            String uid = mAuth.getCurrentUser().getUid();
            final StorageReference storageReference = mStorageRef.child(uid + ".png");
            mUploadTask = storageReference.putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            loadImageToImageView(imageUri);
                            Toast.makeText(getApplicationContext(), "Upload success", Toast.LENGTH_LONG).show();

                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            Double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());

                        }
                    });

        }
    }

    public void loadImageToImageView(Uri uri) {

        Picasso.get()
                .load(uri.toString())
                .fit()
                .centerCrop()
                .into(avatarView);
    }

}
