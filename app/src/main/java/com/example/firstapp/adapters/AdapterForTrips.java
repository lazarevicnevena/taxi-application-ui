package com.example.firstapp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.firstapp.R;
import com.example.firstapp.RideViewActivity;
import com.example.firstapp.model.Trip;
import com.example.firstapp.viewHolders.TripViewHolder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdapterForTrips extends RecyclerView.Adapter<TripViewHolder> {

    private ArrayList<Trip> trips = new ArrayList<Trip>();
    private ArrayList<String> keys = new ArrayList<String>();
    private Context mContext;

    public AdapterForTrips(Context context, ArrayList<Trip> trips, ArrayList<String> keys){
        this.mContext = context;
        this.trips = trips;
        this.keys = keys;
    }

    @Override
    public TripViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.ride_design, viewGroup, false);
        return new TripViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TripViewHolder tripViewHolder, final int i) {
        final String startLocation = trips.get(i).getStartLocation().getAddress().getStreet();
        final String endLocation = trips.get(i).getEndLocation().getAddress().getStreet();

        tripViewHolder.startLocation.setText(startLocation);
        tripViewHolder.startTime.setText(convertLongToDate(trips.get(i).getDate()));
        if(trips.get(i).getDateFinished() == 1L){
            tripViewHolder.endTime.setText("  ------");
        }else{
            tripViewHolder.endTime.setText(convertLongToDate(trips.get(i).getDateFinished()));
        }
        tripViewHolder.targetDestination.setText(endLocation);
        tripViewHolder.price.setText("$ " + trips.get(i).getPrice().toString());
        int index = i + 1;
        tripViewHolder.tripId.setText(mContext.getResources().getString(R.string.trip) + index);

        tripViewHolder.seeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, RideViewActivity.class);
                intent.putExtra("startLocation", startLocation);
                intent.putExtra("endLocation", endLocation);
                intent.putExtra("startTime", convertLongToDate(trips.get(i).getDate()).split(" ")[1]);
                intent.putExtra("startDate", convertLongToDate(trips.get(i).getDate()).split(" ")[0]);
                if(trips.get(i).getDateFinished() == 1L){
                    intent.putExtra("finishTime", "---");
                    intent.putExtra("finishDate", "---");
                }else {
                    intent.putExtra("finishTime", convertLongToDate(trips.get(i).getDateFinished()).split(" ")[1]);
                    intent.putExtra("finishDate", convertLongToDate(trips.get(i).getDateFinished()).split(" ")[0]);
                }
                intent.putExtra("carType", trips.get(i).getCarType());
                intent.putExtra("number", trips.get(i).getPassengerNumber().toString());
                intent.putExtra("price", trips.get(i).getPrice().toString());
                intent.putExtra("tripId", keys.get(i));
                intent.putExtra("index", i + 1);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return trips.size();
    }

    public String convertLongToDate(Long value){
        Date date = new Date(value);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateText = sdf.format(date);
        return  dateText;
    }
}
