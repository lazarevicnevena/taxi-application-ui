package com.example.firstapp.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.ListOfRidesActivity;
import com.example.firstapp.R;
import com.example.firstapp.model.Comment;
import com.example.firstapp.viewHolders.CommentViewHolder;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class AdapterForComments extends RecyclerView.Adapter<CommentViewHolder> {

    private ArrayList<Comment> comments = new ArrayList<Comment>();
    private ArrayList<String> keys = new ArrayList<String>();
    private Context mContext;

    public AdapterForComments(Context context, ArrayList<Comment> comments, ArrayList<String> keys){
        this.comments = comments;
        this.keys = keys;
        mContext = context;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int position) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment_design, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CommentViewHolder holder, final int position) {
        holder.userName.setText(comments.get(position).getUser().getFirstName() + " " + comments.get(position).getUser().getLastName());
        holder.userComment.setText(comments.get(position).getDescription());
        holder.commentTime.setText(convertLongToDate(comments.get(position).getDate()));
        holder.mark.setRating(comments.get(position).getRate());
        holder.deleteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteComment(keys.get(position));
            }
        });
        loadImage(holder.personImage);
    }

    private void deleteComment(String commentId) {
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("Comments").child(commentId);
        databaseReference.removeValue();
        keys.remove(commentId);
    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public String convertLongToDate(Long value){
        Date date = new Date(value);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateText = sdf.format(date);
        return  dateText;
    }

    public void loadImage(final ImageView imageView){
        String currentUserUid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        StorageReference storageReference = FirebaseStorage.getInstance().getReference().child("avatars");
        storageReference.child(currentUserUid + ".png").getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                loadImageToImageView(uri, imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
                Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void loadImageToImageView(Uri uri, ImageView imageView) {

        Picasso.get()
                .load(uri.toString())
                .fit()
                .centerCrop()
                .into(imageView);
    }

}