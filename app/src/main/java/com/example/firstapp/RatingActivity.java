package com.example.firstapp;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.Comment;
import com.example.firstapp.model.Trip;
import com.example.firstapp.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.joda.time.LocalDateTime;

import java.util.ArrayList;

public class RatingActivity extends AppCompatActivity {

    private RatingBar ratingBar;
    private TextView textView;
    private EditText comment;
    private Button submit;
    private User user;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private float userRating;
    private String tripKey;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseComments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        ratingBar = (RatingBar) findViewById(R.id.ratingBar);
        textView = (TextView) findViewById(R.id.value);
        comment = (EditText) findViewById(R.id.comment);
        submit = (Button) findViewById(R.id.submit);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseComments = FirebaseDatabase.getInstance().getReference().child("Comments");

        DatabaseReference userRef = mDatabase.child("Users").child(mAuth.getCurrentUser().getUid());
        userRef.addListenerForSingleValueEvent(new SingleEventCurrentUserReaderListener());

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
                userRating = rating;
                textView.setText("Value: " + rating);
            }
        });

        if(getIntent().hasExtra("tripKey")){
            tripKey = getIntent().getStringExtra("tripKey");
        }

        getData();
    }

    @Override
    protected void onStart() {
        super.onStart();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateForm()){
                    Comment newComment = new Comment();
                    newComment.setDescription(comment.getText().toString());
                    newComment.setDate(System.currentTimeMillis());
                    newComment.setUser(user);
                    newComment.setTripId(tripKey);
                    newComment.setRate(userRating);

                    try {
                        String commentID = mDatabaseComments.push().getKey();
                        mDatabaseComments.child(commentID).setValue(newComment);
                    } catch (Exception e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    onBackPressed();

                }else{
                    Toast.makeText(getApplicationContext(), "Please fill in all fields!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void getData(){
        mImageUrls.add(String.valueOf(R.drawable.crystal));
        mNames.add(getResources().getString(R.string.excellent_service));

        mImageUrls.add(String.valueOf(R.drawable.like));
        mNames.add(getResources().getString(R.string.great_conversation));

        mImageUrls.add(String.valueOf(R.drawable.location));
        mNames.add(getResources().getString(R.string.expert_navigation));

        mImageUrls.add(String.valueOf(R.drawable.glasses));
        mNames.add(getResources().getString(R.string.entertaining_driver));

        mImageUrls.add(String.valueOf(R.drawable.taxi));
        mNames.add(getResources().getString(R.string.cool_car));

        mImageUrls.add(String.valueOf(R.drawable.violin));
        mNames.add(getResources().getString(R.string.awesome_music));

        mImageUrls.add(String.valueOf(R.drawable.cleaningtool));
        mNames.add(getResources().getString(R.string.neat_tidy));

        initRecyclerView();
    }

    private void initRecyclerView(){
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, mNames, mImageUrls);
        recyclerView.setAdapter(adapter);

    }

    public class SingleEventCurrentUserReaderListener implements ValueEventListener {

        public SingleEventCurrentUserReaderListener() {}

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            user = dataSnapshot.getValue(User.class);
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    }

    public boolean validateForm() {
        if(!comment.getText().toString().equals(""))
            return true;
        return false;
    }

}
