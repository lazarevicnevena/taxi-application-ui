package com.example.firstapp.listeners;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.example.firstapp.MapsActivity;
import com.example.firstapp.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

public class MapClickedButtonListener implements OnMapClickListener {

    private Geocoder geocoder;
    private Context context;
    private GoogleMap mMap;

    public MapClickedButtonListener(Geocoder geocoder,
                                    Context context, GoogleMap mMap){
            this.geocoder = geocoder;
            this.context = context;
            this.mMap = mMap;
    }

    @Override
    public void onMapClick(LatLng latLng) {
            if (MapsActivity.userMarker != null){
                MapsActivity.userMarker.remove();
                MapsActivity.userMarker = null;
            }
            MarkerOptions marker = new MarkerOptions().position(latLng);
            try {
                List<Address> address = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);

                String street =  address.get(0).getAddressLine(0);
                marker.title(street.split(",")[0] + ", "+ street.split(",")[1]);
            }catch (IOException e) {
                marker.title("Chosen address");
                e.printStackTrace();
            }
            marker.icon(MapsActivity.bitmapDescriptorFromVector(context, R.mipmap.user_location));
            MapsActivity.userMarkerOptions = marker;
            MapsActivity.userMarker = mMap.addMarker(marker);
            MapsActivity.userMarker.showInfoWindow();

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

    }
}
