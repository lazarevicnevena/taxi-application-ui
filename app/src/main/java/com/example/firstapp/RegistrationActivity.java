package com.example.firstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.firstapp.model.Driver;
import com.example.firstapp.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegistrationActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private DatabaseReference mDatabaseRetrieve;

    private Intent intent;

    private EditText fNameField;
    private EditText lNameField;
    private EditText emailField;
    private EditText phoneNrField;
    private Spinner genderField;
    private EditText pwdField;
    private EditText pwdConfirmField;
    private EditText pwdOldField;
    private ProgressBar progressBar;
    private String pwdVal = "";

    private String oldEmail;
    private String currentUserId;

    private String emailAdmin = "";

    private boolean flagRegistration;
    private String roleRegistration = "";

    private boolean flagUpdateNew;
    private String roleUpdateNew = "";

    private boolean flagPasswordNew;
    private String rolePasswordNew = "";

    private Intent confirmReturnIntent;
    private Intent cancelReturnIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabaseRetrieve = FirebaseDatabase.getInstance().getReference();

        setReferencesToEditFields();

        intent = new Intent(getApplicationContext(), MapsActivity.class);

    }

    @Override
    protected void onResume(){
        super.onResume();

        handleIntentParams();

        if (flagUpdateNew || flagPasswordNew) {
            if (mAuth.getCurrentUser() != null) {
                currentUserId = mAuth.getCurrentUser().getUid();
                oldEmail = mAuth.getCurrentUser().getEmail();
                DatabaseReference ref = mDatabaseRetrieve.child("Users").child(currentUserId);
                ref.addListenerForSingleValueEvent(new SingleEventReaderListener());

                if (flagPasswordNew) {
                    setFieldsWhenPasswordUpdate();
                }
                if (flagUpdateNew) {
                    setFieldsWhenProfileUpdate();
                }
            }
        }

        Button btnConfirm =  findViewById(R.id.btnRegConfirm);
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flagRegistration){
                    if (validateForm()){
                        progressBar.setVisibility(View.VISIBLE);
                        if (roleRegistration.equals("customer")) {
                            mAuth.createUserWithEmailAndPassword(emailField.getText().toString(), pwdField.getText().toString())
                                    .addOnCompleteListener(new RegistrationListener());
                        } else {
                            if (mAuth.getCurrentUser() != null) {
                                emailAdmin = mAuth.getCurrentUser().getEmail();
                            }
                            mAuth.createUserWithEmailAndPassword(emailField.getText().toString(), pwdField.getText().toString())
                                    .addOnCompleteListener(new RegistrationListener1());
                        }
                    }
                } else {
                    if (flagUpdateNew) {
                        progressBar.setVisibility(View.VISIBLE);
                        updateUser();
                    } else if (flagPasswordNew) {
                        final FirebaseUser u = mAuth.getCurrentUser();
                        if (pwdOldField.getText().toString().equals("")) {
                            showToast("Please enter old password in order to change login credentials");
                        } else {
                            if (!oldEmail.equals(emailField.getText().toString())) {
                                final String newEmail = emailField.getText().toString();
                                AuthCredential credential = EmailAuthProvider.getCredential(oldEmail, pwdOldField.getText().toString());
                                progressBar.setVisibility(View.VISIBLE);
                                u.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        u.updateEmail(newEmail)
                                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        if (task.isSuccessful()) {
                                                            if (!pwdField.getText().toString().equals("") && !pwdConfirmField.getText().toString().equals("") &&
                                                                    pwdField.getText().toString().equals(pwdConfirmField.getText().toString())) {
                                                                if (pwdField.getText().toString().length() > 6) {
                                                                    u.updatePassword(pwdField.getText().toString())
                                                                            .addOnSuccessListener(new SuccessfulPasswordChange());
                                                                } else {
                                                                    progressBar.setVisibility(View.INVISIBLE);
                                                                    showToast("New password must contain at least 6 characters!");
                                                                }
                                                            } else {
                                                                progressBar.setVisibility(View.INVISIBLE);
                                                                showToast("Email successfully changed");
                                                                startActivity(confirmReturnIntent);
                                                            }
                                                        } else {
                                                            progressBar.setVisibility(View.INVISIBLE);
                                                            showToast("Could not change email");
                                                        }
                                                    }
                                                });
                                    }
                                });
                            } else if (!pwdField.getText().toString().equals("") && !pwdConfirmField.getText().toString().equals("")){
                                if (pwdField.getText().toString().equals(pwdConfirmField.getText().toString())) {
                                    if (pwdField.getText().toString().length() > 6) {
                                        AuthCredential credential = EmailAuthProvider.getCredential(oldEmail, pwdOldField.getText().toString());
                                        progressBar.setVisibility(View.VISIBLE);
                                        u.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                u.updatePassword(pwdField.getText().toString())
                                                        .addOnSuccessListener(new SuccessfulPasswordChange());
                                            }
                                        });
                                    } else {
                                        progressBar.setVisibility(View.INVISIBLE);
                                        showToast("New passwords must contain at least 6 characters!");
                                    }
                                } else {
                                    progressBar.setVisibility(View.INVISIBLE);
                                    showToast("New passwords must match!");
                                }
                            }
                        }
                    }

                }
            }
        });

        Button btnCancel =  findViewById(R.id.btnRegCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(cancelReturnIntent);
            }
        });
    }

    public class RegistrationListener implements OnCompleteListener<AuthResult> {

        public RegistrationListener(){}

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()){
                Log.d("Email & Password","registration:success");
                String newRole = "Customer";
                if (roleRegistration.equals("driver")) {
                    newRole = "Driver";
                }
                // save user to db;
                User newUser = new User();
                newUser.setFirstName(fNameField.getText().toString());
                newUser.setLastName(lNameField.getText().toString());
                newUser.setPhoneNumber(phoneNrField.getText().toString());
                newUser.setEmail(emailField.getText().toString());
                newUser.setType(newRole);
                newUser.setBalance(2000.0);
                newUser.setGender(genderField.getSelectedItem().toString());
                if (task.getResult() != null) {
                    mDatabase.child("Users").child(task.getResult().getUser().getUid()).setValue(newUser);
                }

                // if admin is registering driver add driver to db
                if (roleRegistration.equals("driver")) {
                    Driver driver = new Driver();
                    driver.setAvailable(true);
                    driver.setUser(newUser);
                    mDatabase.child("Drivers").child(newUser.getPhoneNumber()).setValue(driver);
                }

                progressBar.setVisibility(View.INVISIBLE);
                startActivity(confirmReturnIntent);

            } else {
                progressBar.setVisibility(View.INVISIBLE);
                if (task.getException() != null) {
                    RegistrationActivity.this.showToast(task.getException().getMessage());
                }
            }
        }
    }

    public class RegistrationListener1 implements OnCompleteListener<AuthResult> {

        public RegistrationListener1(){
        }

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()){
                Log.d("Email & Password","registration:success");
                String newRole = "Customer";
                if (roleRegistration.equals("driver")) {
                    newRole = "Driver";
                }
                // save user to db;
                User newUser = new User();
                newUser.setFirstName(fNameField.getText().toString());
                newUser.setLastName(lNameField.getText().toString());
                newUser.setPhoneNumber(phoneNrField.getText().toString());
                newUser.setEmail(emailField.getText().toString());
                newUser.setType(newRole);
                newUser.setBalance(2000.0);
                newUser.setGender(genderField.getSelectedItem().toString());
                if (task.getResult() != null) {
                    mDatabase.child("Users").child(task.getResult().getUser().getUid()).setValue(newUser);
                }

                // if admin is registering driver add driver to db
                if (roleRegistration.equals("driver")) {
                    Driver driver = new Driver();
                    driver.setAvailable(true);
                    driver.setUser(newUser);
                    mDatabase.child("Drivers").child(newUser.getPhoneNumber()).setValue(driver);
                }

                progressBar.setVisibility(View.INVISIBLE);

                reload();


            } else {
                progressBar.setVisibility(View.INVISIBLE);
                if (task.getException() != null) {
                    RegistrationActivity.this.showToast(task.getException().getMessage());
                }
            }
        }
    }

    public class SingleEventReaderListener implements ValueEventListener {

        public SingleEventReaderListener() {}

        @Override
        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);

            fNameField.setText(user.getFirstName());
            lNameField.setText(user.getLastName());
            phoneNrField.setText(user.getPhoneNumber());
            emailField.setText(mAuth.getCurrentUser().getEmail());
            genderField.setSelection(((ArrayAdapter)genderField.getAdapter()).getPosition(user.getGender()));

        }

        @Override
        public void onCancelled(@NonNull DatabaseError databaseError) {

        }
    }

    public boolean validateForm() {
        if (!fNameField.getText().toString().equals("") && !lNameField.getText().toString().equals("") &&
                !emailField.getText().toString().equals("") && !phoneNrField.getText().toString().equals("") &&
                !pwdField.getText().toString().equals("") && !pwdConfirmField.getText().toString().equals("") &&
                !genderField.getSelectedItem().toString().equals("")){
            if (!pwdField.getText().toString().equals(pwdConfirmField.getText().toString())) {
                this.showToast("Password and confirmation password must match!");
                return false;
            }
            if (pwdField.getText().toString().length() < 6) {
                this.showToast("Password must contain at least 6 characters!");
                return false;
            }
            return true;
        } else {
            this.showToast("Please fill in all fields!");
            return false;
        }
    }

    public void showToast(String message) {
        Toast.makeText(RegistrationActivity.this, message,
                Toast.LENGTH_SHORT).show();
    }

    public void updateUser() {
        DatabaseReference ref = mDatabaseRetrieve.child("Users").child(currentUserId);
        if (!fNameField.getText().toString().equals("")) {
            ref.child("firstName").setValue(fNameField.getText().toString());
        }
        if (!lNameField.getText().toString().equals("")) {
            ref.child("lastName").setValue(lNameField.getText().toString());
        }
        if (!rolePasswordNew.equals("driver")) {
            if (!phoneNrField.getText().toString().equals("")) {
                ref.child("phoneNumber").setValue(phoneNrField.getText().toString());
            }
        }
        if (!genderField.getSelectedItem().toString().equals("")) {
            ref.child("gender").setValue(genderField.getSelectedItem().toString());
        }
        progressBar.setVisibility(View.INVISIBLE);

        startActivity(confirmReturnIntent);
    }

    public class SuccessfulPasswordChange implements OnSuccessListener {
        public SuccessfulPasswordChange() {}


        @Override
        public void onSuccess(Object o) {
            progressBar.setVisibility(View.INVISIBLE);
            showToast("Password successfully changed");
            startActivity(confirmReturnIntent);
        }
    }

    public void handleIntentParams() {
        Intent intent = getIntent();
        flagRegistration = false;
        flagUpdateNew = false;
        flagPasswordNew = false;
        pwdVal = "1234567";
        if (intent.hasExtra("com.example.firstapp.REGISTRATION")) {
            flagRegistration = true;
            roleRegistration = intent.getStringExtra("com.example.firstapp.REGISTRATION");
        }
        if (intent.hasExtra("com.example.firstapp.UPDATE")) {
            flagUpdateNew = true;
            roleUpdateNew = intent.getStringExtra("com.example.firstapp.UPDATE");
        }
        if (intent.hasExtra("com.example.firstapp.PASSWORD")) {
            flagPasswordNew = true;
            rolePasswordNew = intent.getStringExtra("com.example.firstapp.PASSWORD");
        }
        if (flagRegistration) {
            if (roleRegistration.equals("customer")) {
                confirmReturnIntent = new Intent(getApplicationContext(), MapsActivity.class);
            } else if (roleRegistration.equals("driver")) {
                confirmReturnIntent = new Intent(getApplicationContext(), AdminHomeActivity.class);
            }
        }
        if (flagUpdateNew) {
            if (roleUpdateNew.equals("customer")) {
                confirmReturnIntent = new Intent(getApplicationContext(), MapsActivity.class);
            } else if (roleUpdateNew.equals("driver")) {
                confirmReturnIntent = new Intent(getApplicationContext(), DriverHomeActivity.class);
            } else if (roleUpdateNew.equals("admin")) {
                confirmReturnIntent = new Intent(getApplicationContext(), AdminHomeActivity.class);
            }
        }
        if (flagPasswordNew) {
            if (rolePasswordNew.equals("customer")) {
                confirmReturnIntent = new Intent(getApplicationContext(), MapsActivity.class);
            } else if (rolePasswordNew.equals("driver")) {
                confirmReturnIntent = new Intent(getApplicationContext(), DriverHomeActivity.class);
            } else if (rolePasswordNew.equals("admin")) {
                confirmReturnIntent = new Intent(getApplicationContext(), AdminHomeActivity.class);
            }
        }
        if (flagRegistration && roleRegistration.equals("customer")) {
            cancelReturnIntent = new Intent(getApplicationContext(), LoginActivity.class);
        } else {
            cancelReturnIntent = confirmReturnIntent;
        }

    }

    public void setReferencesToEditFields() {
        fNameField = findViewById(R.id.firstNameReg);
        lNameField = findViewById(R.id.lastNameReg);
        emailField = findViewById(R.id.emailReg);
        phoneNrField = findViewById(R.id.phoneNrReg);
        pwdField = findViewById(R.id.pwdReg);
        pwdConfirmField = findViewById(R.id.pwdConfirmReg);
        genderField = findViewById(R.id.spinner);
        pwdOldField = findViewById(R.id.pwdOldReg);
        progressBar = findViewById(R.id.progress_barReg);
    }

    public void setFieldsWhenPasswordUpdate() {
        findViewById(R.id.firstNameRegTxt).setVisibility(View.GONE);
        fNameField.setVisibility(View.GONE);
        findViewById(R.id.lastNameRegTxt).setVisibility(View.GONE);
        lNameField.setVisibility(View.GONE);
        findViewById(R.id.phoneNrRegTxt).setVisibility(View.GONE);
        phoneNrField.setVisibility(View.GONE);
        findViewById(R.id.pwdOldRegTxt).setVisibility(View.VISIBLE);
        pwdOldField.setVisibility(View.VISIBLE);
        findViewById(R.id.spinnerTxt).setVisibility(View.GONE);
        genderField.setVisibility(View.GONE);
        ((TextView)findViewById(R.id.pwdRegTxt)).setText(RegistrationActivity.this.getResources().getString(R.string.new_password));
    }

    public void setFieldsWhenProfileUpdate() {
        findViewById(R.id.emailRegTxt).setVisibility(View.GONE);
        findViewById(R.id.emailReg).setVisibility(View.GONE);
        findViewById(R.id.pwdRegTxt).setVisibility(View.GONE);
        findViewById(R.id.pwdReg).setVisibility(View.GONE);
        findViewById(R.id.pwdConfirmRegTxt).setVisibility(View.GONE);
        findViewById(R.id.pwdConfirmReg).setVisibility(View.GONE);
        if (roleUpdateNew.equals("driver")) {
            findViewById(R.id.phoneNrRegTxt).setVisibility(View.GONE);
            phoneNrField.setVisibility(View.GONE);
        }
    }

    public void reload() {
        mAuth.signOut();
        mAuth.signInWithEmailAndPassword(emailAdmin, pwdVal)
                .addOnCompleteListener(new LoginListener());
    }

    public class LoginListener implements OnCompleteListener<AuthResult> {

        public LoginListener(){}

        @Override
        public void onComplete(@NonNull Task<AuthResult> task) {
            if (task.isSuccessful()){
                startActivity(confirmReturnIntent);
            } else {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        }
    }

}
